import { idLiteralToMap, enumToIds } from "../src/utils/enumUtils";

describe("Tests of idLiteralToMap", function(){
    it("array of values", function() {
        const array = [{_id: "123", literal: "123 literal"}, {_id: "456", literal: "456 literal"}];
        const map = idLiteralToMap(array);
        expect(map["123 literal"]).toEqual("123");
        expect(map["456 literal"]).toEqual("456");
    })
});

describe("Tests of enumToIds", function(){
    it("enumToIds", async function(){
        const model = {
            find: function(selection, projection){
                return {
                    exec: async function(){
                        return [{_id: "123", literal: "123 literal"}, {_id: "456", literal: "456 literal"}];
                    }
                };
            }
        };
        const map = await enumToIds(model);
        expect(map["123 literal"]).toEqual("123");
        expect(map["456 literal"]).toEqual("456");
    })
});