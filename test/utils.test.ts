import { nonNullOrEmpty } from "../src/utils/utils";

describe("Tests of nonNullOrEmpty", function () {
    it("all the nulls and empty objects", function() {
        expect(nonNullOrEmpty("")).toBeFalsy(); // ""
        expect(nonNullOrEmpty([])).toBeFalsy(); // []
        expect(nonNullOrEmpty(null)).toBeFalsy(); // null
        expect(nonNullOrEmpty(undefined)).toBeFalsy();// undefined
    });

    it("ok strings", function() {
        expect(nonNullOrEmpty("Hello")).toBeTruthy(); // Regular strings
        expect(nonNullOrEmpty("שלום")).toBeTruthy(); // UTF-8
        expect(nonNullOrEmpty("\n\r")).toBeTruthy(); // Special characters
    })

    it("ok arrays", function() {
        expect(nonNullOrEmpty(["Hello"])).toBeTruthy(); // Simple singletone array with a simple string
        expect(nonNullOrEmpty(["🎈", 1, 2, 3])).toBeTruthy(); // UTF-8, string and numbers
        expect(nonNullOrEmpty([null, undefined])).toBeTruthy(); // an array of null and undefined is not null nor empty
    })
});