import { Classification } from "../models/Models";

export async function createClassification(_, { literal, string, code }) {
    const newClassification = new Classification({literal, string, code});
    await newClassification.save();
    return newClassification;
}

export async function updateClassification(_, { literal, string, code }) {
    const newClassification = await Classification.findOneAndUpdate({literal: literal}, {literal, string, code}, {new: true});

    // If no roles have been updated, throw an exception
    if (!newClassification){
        throw `Classification enum: "${JSON.stringify({ literal, string, code })}" does not exists in the database and thus cannot be updated`;
    }

    return newClassification;
}

export function fetchAllClassifications() {
    return Classification.find()
}