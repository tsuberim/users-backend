import { User, Role, Classification } from "../models/Models";
import { enumToIds } from "../utils/enumUtils";

async function convertUserToDbType({ user, name, classification, roles, remark }){
    const rolesMap = await enumToIds(Role);
    const roleIds = roles.map(r => rolesMap[r]);
    if (roleIds.includes(undefined)){
        throw {
            message: "Invalid role in roles list",
            extra: {
                roles: JSON.stringify(roles.filter(r => !rolesMap[r]))
            }};
    }

    const classificationMap = await enumToIds(Classification);
    const classificationId = classificationMap[classification];
    if (!classificationId){
        throw {
             message: `Invalid classification "${classification}"`,
             extra: {
                 classification: classification
             }
        };
    }

    return {user, name, classification: classificationId, roles: roleIds, remark};
}


export async function createUser(_, { user, name, classification, roles, remark }) {
    const newUserObject = await convertUserToDbType({ user, name, classification, roles, remark });
    const newUser = await new User(newUserObject).save();
    return await newUser.populate("classification roles").execPopulate();
}

export async function updateUser(_, { user, name, classification, roles, remark }) {
    const newUserObject = await convertUserToDbType({ user, name, classification, roles, remark });

    const updatedUser = await User.findOneAndUpdate({user: user}, newUserObject, {new: true});

    // If no users have been updated, throw an exception
    if (!updatedUser){
        throw `User "${user}" does not exists in the database and thus cannot be updated`;
    }

    return await updatedUser.populate("classification roles").execPopulate();
}

export async function deleteUser(_, { user }) {
    const deletedUser = await User.findOneAndDelete({ user: user });

    if (!deletedUser){
        throw `User ${user} does not exists in the database and thus cannot be deleted`;
    }

    return await deletedUser.populate("classification roles").execPopulate();
}

export function fetchAllUsers() {
    return User.find().populate("classification roles");
}