import { Role } from "../models/Models";

export async function createRole(_, { literal, string, code }) {
    const newRole = new Role({literal, string, code});
    await newRole.save();
    return newRole;
}

export async function updateRole(_, { literal, string, code }) {
    const newRole = await Role.findOneAndUpdate({literal: literal}, {literal, string, code}, {new: true});

    // If no roles have been updated, throw an exception
    if (!newRole){
        throw `Role enum: "${JSON.stringify({ literal, string, code })}" does not exists in the database and thus cannot be updated`;
    }

    return newRole;
}

export function fetchAllRoles() {
    return Role.find()
}