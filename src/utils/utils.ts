export function nonNullOrEmpty(obj: string | unknown[]): boolean {
    return !!obj && obj.length > 0;
}