export function idLiteralToMap(array) {
    const map = new Map();
    array.forEach(element => {
        map[element.literal] = element._id;
    });
    return map;
}

export async function enumToIds(model){
    const docs = await model.find({}, "_id literal").exec();
    return idLiteralToMap(docs);
}