// Import mongoose and create a Schema
import mongoose from "mongoose";
import { nonNullOrEmpty } from "../utils/utils";

// Schema of the enum documents
const EnumSchema = new mongoose.Schema({
    literal: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: nonNullOrEmpty,
            message: "enum's literal cannot be empty"
        }
    },
    string: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: nonNullOrEmpty,
            message: "enum's string cannot be empty"
        }
    }
});

export const Classification = mongoose.model("ClassificationSchema", EnumSchema);
export const Role = mongoose.model("RoleSchema", EnumSchema);

// Schema of the User document
const UserSchema = new mongoose.Schema({
    user: {
        type: String,
        required: true,
        unique: true,
        validate : {
            validator: nonNullOrEmpty,
            message: "User's identifier cannot be null or empty"
        }
    },
    name: {
        type: String,
        required: true,
        validate : {
            validator : nonNullOrEmpty,
            message: "User's name cannot be null or empty"
        }
    },
    classification: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ClassificationSchema",
        required: true
    },
    roles: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: "RoleSchema" }],
        required: true,
        validate: {
            validator: nonNullOrEmpty,
            message: "User's roles cannot be null or empty"
        }
    },
    remark: {
        type: String
    }  
});

export const User = mongoose.model("User", UserSchema);