import { ApolloServer } from "apollo-server-express";
import express from "express";
import mongoose from "mongoose";
import { resolvers } from "./graphql/resolvers";
import { typeDefs } from "./graphql/typeDefs";
import bunyan from 'bunyan';

const log = bunyan.createLogger({name: "worker-log"});

mongoose.set("debug", (collectionName, method, query, doc) => {
    log.info(`${collectionName}.${method}`, JSON.stringify(query), doc);
});

export const server = new ApolloServer({
    typeDefs,
    resolvers
});

const startServer = async () => {
    //if (!process.env.DB_CONNECTION_STRING || !process.env.PORT) return;
    
    // Getting the db connection string and port from environment variables
    const port = 3000; // parseInt(process.env.PORT);
    const connnectionString = "mongodb://localhost:27017/?readPreference=primary&ssl=false"; // string = process.env.DB_CONNECTION_STRING;

    const app = express();

    server.applyMiddleware({ app });
      
    // Connecting to the database
    log.info(`Connecting to the db at address: ${connnectionString}`);
    await mongoose.connect(connnectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    });


    log.info(`Successfully connected to the database`);

    // Starting the server
    log.info(`Starting the server on port ${port}`);
    app.listen({ port: port }, () => {
        log.info(`The server is ready`);
    });
};

startServer();