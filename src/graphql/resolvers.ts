import * as userActions from "../actions/userActions";
import * as roleActions from "../actions/roleActions";
import * as classificationActions from "../actions/classificationActions";

export const resolvers = {
    Query: {
        users: userActions.fetchAllUsers,
        roles: roleActions.fetchAllRoles,
        classifications: classificationActions.fetchAllClassifications
    },
    Mutation: {
        createUser: userActions.createUser,
        updateUser: userActions.updateUser,
        deleteUser: userActions.deleteUser,

        createRole: roleActions.createRole,
        updateRole: roleActions.updateRole,

        createClassification: classificationActions.createClassification,
        updateClassification: classificationActions.updateClassification,
    }
};