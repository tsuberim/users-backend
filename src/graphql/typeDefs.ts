import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type BinatEnum {
    literal: String!
    string: String!
  }

  type User {
    user: String!
    name: String!
    classification: BinatEnum!
    roles: [BinatEnum!]!
    remark: String
  }

  type Query {
    users: [User!]!,
    classifications: [BinatEnum!]!
    roles: [BinatEnum!]!
  }

  type Mutation {
    createUser(user: String!, name: String!, classification: String!, roles: [String!]!, remark: String): User!
    updateUser(user: String!, name: String!, classification: String!, roles: [String!]!, remark: String): User!
    deleteUser(user: String!): User!

    createClassification(literal: String!, string: String!): BinatEnum!
    updateClassification(literal: String!, string: String!): BinatEnum!

    createRole(literal: String!, string: String!): BinatEnum!
    updateRole(literal: String!, string: String!): BinatEnum!
  }
`;